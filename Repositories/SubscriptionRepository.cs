﻿using Microsoft.EntityFrameworkCore;
using subscription_service.Context;
using subscription_service.Entities;

namespace subscription_service.Repositories
{
    public class SubscriptionRepository : ISubscriptionRepository
    {
        private readonly SubscriptionServiceDbContext subscriptionServiceDbContext;

        public SubscriptionRepository(SubscriptionServiceDbContext subscriptionServiceDbContext)
        {
            this.subscriptionServiceDbContext = subscriptionServiceDbContext;
        }

        public async Task<IEnumerable<Subscriber>> FindSubscribersByEventNameIs(string eventName)
        {
            var result = from events in subscriptionServiceDbContext.Events
                         join eventsSubscribers in subscriptionServiceDbContext.EventsSubscribers
                             on events.Id equals eventsSubscribers.EventId
                         join subscribers in subscriptionServiceDbContext.Subscribers
                             on eventsSubscribers.SubscriberId equals subscribers.Id
                         where events.Name == eventName
                         select subscribers;

            return await result.ToListAsync();
        }

        public async Task<Event> AddEventsSubscribers(List<EventsSubscribers> eventsSubscribers)
        {
            int id = 0;

            foreach(var eventSubscriber in eventsSubscribers)
            {
                await subscriptionServiceDbContext.EventsSubscribers.AddAsync(eventSubscriber);
                id = eventSubscriber.EventId;
            }

            return await subscriptionServiceDbContext.Events.FindAsync(id);
        }

        public async Task<Event> RemoveEventsSubscribers(List<EventsSubscribers> eventsSubscribers)
        {
            int id = 0;

            foreach (var eventSubscriber in eventsSubscribers)
            {
                subscriptionServiceDbContext.EventsSubscribers.Remove(eventSubscriber);
                id = eventSubscriber.EventId;
            }

            return await subscriptionServiceDbContext.Events.FindAsync(id);
        }

        public async Task<Event> FindEventById(int id)
        {
            return await subscriptionServiceDbContext.Events.FindAsync(id);
        }

        public async Task<Subscriber> FindSubscriberById(int id)
        {
            return await subscriptionServiceDbContext.Subscribers.FindAsync(id);
        }

        public Task<Event> AddEventsSubscribers(IEnumerable<EventsSubscribers> eventsSubscribers)
        {
            throw new NotImplementedException();
        }

        public Task<Event> RemoveEventsSubscribers(IEnumerable<EventsSubscribers> eventsSubscribers)
        {
            throw new NotImplementedException();
        }
    }
}

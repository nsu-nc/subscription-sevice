﻿using subscription_service.Entities;

namespace subscription_service.Repositories
{
    public interface ISubscriptionRepository
    {
        Task<IEnumerable<Subscriber>> FindSubscribersByEventNameIs(string EventName);
        Task<Event> FindEventById(int id);
        Task<Subscriber> FindSubscriberById(int id);
        Task<Event> AddEventsSubscribers(IEnumerable<EventsSubscribers> eventsSubscribers);
        Task<Event> RemoveEventsSubscribers(IEnumerable<EventsSubscribers> eventsSubscribers);
    }
}

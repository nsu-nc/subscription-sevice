﻿using Microsoft.EntityFrameworkCore;
using subscription_service.Entities;

namespace subscription_service.Context
{
    public class SubscriptionServiceDbContext : DbContext
    {
        public DbSet<Event> Events { get; set; }
        public DbSet<EventsSubscribers> EventsSubscribers { get; set; }
        public DbSet<History> Histories { get; set; }
        public DbSet<Subscriber> Subscribers { get; set; }

        public SubscriptionServiceDbContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EventConfiguration());
            modelBuilder.ApplyConfiguration(new EventsSubscribersConfiguration());
            modelBuilder.ApplyConfiguration(new HistoryConfiguration());
            modelBuilder.ApplyConfiguration(new SubscriberConfiguration());
        }
    }
}

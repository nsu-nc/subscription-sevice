﻿using subscription_service.Entities;
using subscription_service.DTOs;
using System.Xml.Linq;

namespace subscription_service.Mapping
{
    public class EventSubscriptionMapper
    {
        public Event EventDTOtoEvent(EventDTO dto)
        {
            var @event = new Event()
            {
                Deleted = dto.Deleted,
                Id = dto.Id,
                Name = dto.Name,
                SubAbitily = dto.SubAbitily,
            };

            var eventsSubscribers = new List<EventsSubscribers>();
            foreach (var eventsSubscriber in dto.EventsSubscribers)
            {
                var subcscriber = SubscriberDTOtoSubscriber(eventsSubscriber.Subscriber);

                eventsSubscribers.Add(new EventsSubscribers()
                {
                    SubscriberId = subcscriber.Id,
                    Subscriber = subcscriber,
                    Event = @event,
                    EventId = @event.Id,
                });
            }

            @event.EventsSubscribers = eventsSubscribers;

            return @event;
        }

        public EventDTO EventToEventDTO(Event @event)
        {
            var eventDTO = new EventDTO
            {
                Deleted = @event.Deleted,
                SubAbitily = @event.SubAbitily,
                Id = @event.Id,
                Name = @event.Name,
            };

            var eventSubs = new List<EventSubscriberDTO>();
            foreach(var eventSubscriber in @event.EventsSubscribers)
            {
                var subscriber = SubscriberToSubscriberDTO(eventSubscriber.Subscriber);

                eventSubs.Add(new EventSubscriberDTO
                {
                    Subscriber = subscriber,
                });
            }

            return eventDTO;
        }

        public History HistoryDTOtoHistory(HistoryDTO dto)
        {
            return new History();
        }

        public Subscriber SubscriberDTOtoSubscriber(SubscriberDTO dto)
        {
            return new Subscriber()
            {
                Id = dto.Id,
                Mail = dto.Mail,
            };
        }

        public SubscriberDTO SubscriberToSubscriberDTO(Subscriber subscriber)
        {
            return new SubscriberDTO()
            {
                Id = subscriber.Id,
                Mail = subscriber.Mail,
            };
        }
    }
}
        
            





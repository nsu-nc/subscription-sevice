﻿using subscription_service.DTOs;
using subscription_service.Entities;
using subscription_service.Mapping;
using subscription_service.Repositories;
using System.Reflection.Metadata.Ecma335;

namespace subscription_service.Services
{
    public class SubscriptionService : ISubscriptionService
    {
        private readonly ISubscriptionRepository subscriptionRepository;

        public SubscriptionService(ISubscriptionRepository subscriptionRepository)
        {
            this.subscriptionRepository = subscriptionRepository;
        }

        public async Task<IEnumerable<SubscriberDTO>> FindSubscribersByEvent(EventDTO dto)
        {
            var result = await subscriptionRepository.FindSubscribersByEventNameIs(dto.Name);

            var mapper = new EventSubscriptionMapper();

            var subscribers = new List<SubscriberDTO>();
            foreach (var subscriber in result)
            {
                subscribers.Add(mapper.SubscriberToSubscriberDTO(subscriber));
            }

            return subscribers;
        }

        public async Task<EventDTO> Subscribe(SubInfoDTO subInfoDTO)
        {
            var eventsSubscribers = await GetEventsSubscribers(subInfoDTO);
            var result = await subscriptionRepository.AddEventsSubscribers(eventsSubscribers);

            var mapper = new EventSubscriptionMapper();

            return mapper.EventToEventDTO(result);
        }

        public async Task<EventDTO> Unsubscribe(SubInfoDTO subInfoDTO)
        {
            var eventsSubscribers = await GetEventsSubscribers(subInfoDTO);
            var result = await subscriptionRepository.RemoveEventsSubscribers(eventsSubscribers);

            var mapper = new EventSubscriptionMapper();

            return mapper.EventToEventDTO(result);
        }

        private async Task<IEnumerable<EventsSubscribers>> GetEventsSubscribers(SubInfoDTO subscInfoDTO)
        {
            var mapper = new EventSubscriptionMapper();
            var @event = await subscriptionRepository.FindEventById(subscInfoDTO.Event.Id);

            var eventsSubscribers = new List<EventsSubscribers>();
            foreach (var subscriber in subscInfoDTO.Subscribers)
            {
                var sub = await subscriptionRepository.FindSubscriberById(subscriber.Id);

                eventsSubscribers.Add(new EventsSubscribers
                {
                    Event = @event,
                    EventId = @event.Id,
                    Subscriber = sub,
                    SubscriberId = sub.Id,
                });
            }

            return eventsSubscribers;
        }
    }
}

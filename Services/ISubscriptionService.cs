﻿using subscription_service.DTOs;
using subscription_service.Entities;

namespace subscription_service.Services
{
    public interface ISubscriptionService
    {
        Task<IEnumerable<SubscriberDTO>> FindSubscribersByEvent(EventDTO dto);
        Task<EventDTO> Subscribe(SubInfoDTO subInfoDTO);
        Task<EventDTO> Unsubscribe(SubInfoDTO subInfoDTO);

    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace subscription_service.Entities
{
    public class Event
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public bool SubAbitily { get; set; }
        public bool Deleted { get; set; }
        public ICollection<History> Histories { get; set; } = new List<History>();
        public ICollection<EventsSubscribers> EventsSubscribers { get; set; } = new List<EventsSubscribers>();
    }

    public class EventConfiguration : IEntityTypeConfiguration<Event> { 
        public void Configure(EntityTypeBuilder<Event> builder)
        {
            builder
                .HasKey(e => e.Id);
            builder
                .Property(e => e.Id)
                .HasColumnName("id");
            builder
                .Property(e => e.Name)
                .HasColumnName("name");
            builder
                .Property(e => e.SubAbitily)
                .HasColumnName("sub_ability");
            builder
                .Property(e => e.Deleted)
                .HasColumnName("deleted");

        }
    }
}

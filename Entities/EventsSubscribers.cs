﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace subscription_service.Entities
{
    public class EventsSubscribers
    {
        public int EventId { get; set; }
        public Event? Event { get; set; }
        public int SubscriberId { get; set; }
        public Subscriber? Subscriber { get; set; }

    }

    public class EventsSubscribersConfiguration : IEntityTypeConfiguration<EventsSubscribers>
    {
        public void Configure(EntityTypeBuilder<EventsSubscribers> builder)
        {
            builder
                .Property(es => es.EventId)
                .HasColumnName("event_id");
            builder
                .Property(es => es.SubscriberId)
                .HasColumnName("subscriber_id");
            builder
                .HasKey(es => new { es.EventId, es.SubscriberId });
            builder
                .HasOne(es => es.Event)
                .WithMany(e => e.EventsSubscribers)
                .HasForeignKey(e => e.EventId);
            builder
                .HasOne(es => es.Subscriber)
                .WithMany(e => e.EventsSubscribers)
                .HasForeignKey(e => e.SubscriberId);
        }
    }
}

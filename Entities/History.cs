﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace subscription_service.Entities
{
    public class History
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public Event Event { get; set; }
        public string? EventType { get; set; }
        public string? Discriprion { get; set; }
    }

    public class HistoryConfiguration : IEntityTypeConfiguration<History>
    {
        public void Configure(EntityTypeBuilder<History> builder)
        {
            builder
                .Property(h => h.Id)
                .HasColumnName("Id");
            builder
                .Property(h => h.EventId)
                .HasColumnName("event_id");
            builder
                .Property(h => h.EventType)
                .HasColumnName("event_type");
            builder
                .Property(h => h.Discriprion)
                .HasColumnName("discription");
            builder
                .HasKey(e => e.Id);
            builder
                .HasOne(h => h.Event)
                .WithMany(e => e.Histories)
                .HasForeignKey(h => h.EventId);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace subscription_service.Entities
{
    public class Subscriber
    {
        public int Id { get; set; }
        public string? Mail { get; set; }
        public ICollection<EventsSubscribers> EventsSubscribers { get; set; }
    }

    public class SubscriberConfiguration : IEntityTypeConfiguration<Subscriber>
    {
        public void Configure(EntityTypeBuilder<Subscriber> builder)
        {
            builder
                .Property(s => s.Id)
                .HasColumnName("id");
            builder
                .Property(s => s.Mail)
                .HasColumnName("email");
            builder
                .HasKey(s => s.Id);
        }
    }
}

﻿using subscription_service.Entities;
using subscription_service.DTOs;

namespace subscription_service.DTOs
{
    public class HistoryDTO
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public string? EventType { get; set; }
        public string? Discriprion { get; set; }
    }
}

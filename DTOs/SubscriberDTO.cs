﻿namespace subscription_service.DTOs
{
    public class SubscriberDTO
    {
        public int Id { get; set; }
        public string? Mail { get; set; }
    }
}

﻿namespace subscription_service.DTOs
{
    public class SubInfoDTO
    {
        public EventDTO Event { get; set; }
        public ICollection<SubscriberDTO> Subscribers { get; set; }
    }
}

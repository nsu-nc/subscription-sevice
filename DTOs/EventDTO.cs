﻿using subscription_service.Entities;

namespace subscription_service.DTOs
{
    public class EventDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public bool SubAbitily { get; set; }
        public bool Deleted { get; set; }
        public ICollection<EventSubscriberDTO> EventsSubscribers { get; set; } = new List<EventSubscriberDTO>();
    }
}

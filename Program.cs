using System.Reflection;
using Microsoft.EntityFrameworkCore;
using subscription_service.Context;
using subscription_service.Repositories;
using subscription_service.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

builder.Services.AddDbContext<SubscriptionServiceDbContext>(options =>
    options.UseNpgsql(
        builder.Configuration
        .GetConnectionString("SubscriptionServiceConnectionString")), ServiceLifetime.Scoped);

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(s =>
{
    s.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
    {
        Version = "v1",
        Title = "SubscriprionServiceAPI",
        Description = "API for subscription-service"
    });

    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    s.IncludeXmlComments(xmlPath);
});

builder.Services.AddScoped<ISubscriptionRepository, SubscriptionRepository>();
builder.Services.AddScoped<ISubscriptionService, SubscriptionService>();


var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "SubscriprionService API V");
    c.RoutePrefix = string.Empty;
});


app.MapControllers();
app.Run();

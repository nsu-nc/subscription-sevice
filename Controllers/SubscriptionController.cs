﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using subscription_service.DTOs;
using subscription_service.Entities;
using subscription_service.Services;
using System.Net;

namespace subscription_service.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class SubscriptionController : Controller
    {
        private readonly ISubscriptionService subscriptionService;

        public SubscriptionController(ISubscriptionService subscriptionService)
        {
            this.subscriptionService = subscriptionService;
        }

        /// <summary>
        /// Returns a list of subscribers of a concrete event
        /// </summary>
        /// <returns> returns a list of subscribers </returns>
        /// <response code="200"> returns a list of subscribers </response>
        /// <response code="400"> return error message </response>
        
        [Produces("application/json")]
        //[ProducesResponseType(typeof(RequestError), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(List<Subscriber>), StatusCodes.Status200OK)]
        [HttpPost]
        public async Task<IActionResult> Find(EventDTO dto)
        {
            var result = await subscriptionService.FindSubscribersByEvent(dto);

            return Ok(result);
        }

        /// <summary>
        /// Subscribes user to a concrete event
        /// </summary>
        /// <returns> returns an event entity with full metainformation about it </returns>
        /// <response code="200"> returns an event entity with full metainformation about it </response>
        /// <response code="400"> return error message </response>
        
        [Produces("application/json")]
        //[ProducesResponseType(typeof(RequestError), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Event), StatusCodes.Status200OK)]
        [HttpPost]
        public async Task<IActionResult> Subscribe(SubInfoDTO dto)
        {
            var result = await subscriptionService.Subscribe(dto);

            return Ok(result);
        }

        /// <summary>
        /// Unsubscribe user from a concrete event
        /// </summary>
        /// <returns> returns an event entity with full metainformation about it </returns>
        /// <response code="200"> returns an event entity with full metainformation about it </response>
        /// <response code="400"> return error message </response>
        
        [Produces("application/json")]
        //[ProducesResponseType(typeof(RequestError), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Event), StatusCodes.Status200OK)]
        [HttpPost]
        public async Task<IActionResult> Unsubscribe(SubInfoDTO dto)
        {
            var result = await subscriptionService.Unsubscribe(dto);

            return Ok(result);
        }
    }
}
